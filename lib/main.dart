
import 'package:blooddb/providers/navigationprovider.dart';
import 'package:blooddb/ui/mainscreen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';


void main(){

  runApp(new MaterialApp(
    title: 'Blood DB',
      theme: ThemeData(
        // Define the default brightness and colors.
        brightness: Brightness.light,
        primaryColor: Color(0xffde0a1e),
        accentColor: Color(0xffee848e),

        // Define the default font family.
        fontFamily: 'Montserrat',

        // Define the default TextTheme. Use this to specify the default
        // text styling for headlines, titles, bodies of text, and more.
        textTheme: TextTheme(
          headline: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
          title: TextStyle(fontSize: 36.0, fontStyle: FontStyle.italic),
          body1: TextStyle(fontSize: 14.0, fontFamily: 'Hind'),

        ),

      ),
     home: ChangeNotifierProvider<BottomNavigationProvider>(
        child: MainScreen(),
        builder: (BuildContext context) => BottomNavigationProvider(),
      ),
    //home: MainScreen()

  ));
}