import 'package:blooddb/providers/dropdownprovider.dart';
import 'package:blooddb/providers/navigationprovider.dart';
import 'package:blooddb/ui/donors.dart';
import 'package:blooddb/ui/requests.dart';

import 'package:fancy_bottom_navigation/fancy_bottom_navigation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:unicorndial/unicorndial.dart';
import 'groupBtn.dart';

class MainScreen extends StatelessWidget {
  TextEditingController _textFieldController = TextEditingController();

  _addUSer(BuildContext context) async
  { double maxWidth = MediaQuery.of(context).size.width * 0.5;
     return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(32.0)),

            ),
            title: Center(child: new Text('Subscribe as a donor',style: new TextStyle(fontSize: 14.0),)),
            content:SizedBox(

              height: MediaQuery.of(context).size.height/2,
              width: double.infinity,
              child: Padding(
                padding: const EdgeInsets.all(5.0),
                child: new ListView(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(15.0),
                        child: Container(
                          color: Color(0xfff5f5f5),
                          child: ListTile(
                            title: TextField(
                              decoration: new InputDecoration(
                                  border: InputBorder.none,
                                  focusedBorder: InputBorder.none,
                                  contentPadding: EdgeInsets.only(
                                      left: 15, bottom: 11, top: 11, right: 15),
                                  hintText: 'Name',
                                  hintStyle: new TextStyle(fontSize: 14.0)),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(15.0),
                        child: Container(
                          color: Color(0xfff5f5f5),
                          child: ListTile(
                            title: TextField(
                              decoration: new InputDecoration(
                                  border: InputBorder.none,
                                  focusedBorder: InputBorder.none,
                                  contentPadding: EdgeInsets.only(
                                      left: 15, bottom: 11, top: 11, right: 15),
                                  hintText: 'Phone',
                                  hintStyle: new TextStyle(fontSize: 14.0)),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(15.0),
                        child: Container(
                          color: Color(0xfff5f5f5),
                          child: ListTile(
                            title: TextField(
                              decoration: new InputDecoration(
                                  border: InputBorder.none,
                                  focusedBorder: InputBorder.none,
                                  contentPadding: EdgeInsets.only(
                                      left: 15, bottom: 11, top: 11, right: 15),
                                  hintText: 'Location',
                                  hintStyle: new TextStyle(fontSize: 14.0)),
                            ),
                          ),
                        ),
                      ),

                    ),


                    ShowSelectRadio(),

                    Padding(
                      padding: const EdgeInsets.only(left:8.0,right: 8.0,top: 25.0),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(15.0),
                        child: SizedBox(
                          width: double.infinity,
                          height: 45.0,
                          child: RaisedButton(
                            color: Color(0xffde0a1e),
                            child: Text("Confirm",style: new TextStyle(
                                color: Colors.white,
                                fontSize: 20.0
                            ),),
                            onPressed: (){
                              Navigator.of(context).pop();
                            },
                          ),
                        ),
                      ),
                    )

                  ],

                ),
              ),
            ) ,

          );
        });

  }

  @override
  Widget build(BuildContext context) {
    var currentTab = [
      ChangeNotifierProvider<DropDownProvider>(
        child: RequestScreens(),
        builder: (BuildContext context) => DropDownProvider(),
      ),
      ChangeNotifierProvider<DropDownProvider>(
        child: DonorsScreens(),
        builder: (BuildContext context) => DropDownProvider(),
      ),
    ];
    var provider = Provider.of<BottomNavigationProvider>(context);
    // TODO: implement build
    return Scaffold(
      floatingActionButton: UnicornDialer(
        parentButtonBackground: Colors.red[700],
        orientation: UnicornOrientation.VERTICAL,
        parentButton: Icon(Icons.assignment),
        childButtons: _getProfileMenu(context),
      ),
      body: currentTab[provider.currentIndex],
      bottomNavigationBar: FancyBottomNavigation(
          tabs: [
            TabData(
                iconData: Icons.airline_seat_individual_suite,
                title: "Requests"),
            TabData(iconData: Icons.person, title: "Donars")
          ],
          onTabChangedListener: (index) {
            provider.currentIndex = index;
          }),
    );
  }

  List<UnicornButton> _getProfileMenu(BuildContext context) {
    List<UnicornButton> children = [];

    // Add Children here
    children.add(_profileOption(iconData: Icons.person_add, onPressed:() {

_addUSer(context) ;
    }));
    children.add(_profileOption(iconData: Icons.invert_colors, onPressed: (){}));

    return children;
  }

  Widget _profileOption({IconData iconData, Function onPressed}) {
    return UnicornButton(
        currentButton: FloatingActionButton(
          backgroundColor: Colors.red[500],
          //mini: true,
          child: Icon(iconData,color: Colors.white,),
          onPressed: onPressed,
        ));
  }



}
