import 'package:flutter/material.dart';

class ShowSelectRadio extends StatefulWidget {
  @override
  ShowSelectRadioState createState() {
    return new ShowSelectRadioState();
  }
}

class ShowSelectRadioState extends State<ShowSelectRadio> {
  String dropdownValue = 'Select the Blood Type';

  @override
  Widget build(BuildContext context) {

   return

     Padding(
       padding: const EdgeInsets.only(left:8.0,right: 8.0),
       child: ClipRRect(
         borderRadius: BorderRadius.circular(15.0),
         child: Container(
         color: Color(0xfff5f5f5),
         child: ListTile(
           title: DropdownButton<String>(
             value: dropdownValue,

             style: TextStyle(
                 fontSize: 14.0,
               color: Colors.black
             ),
             underline: Container(
               height: 2,
               color: Colors.redAccent,
             ),
             onChanged: (String newValue) {
               setState(() {
                 dropdownValue = newValue;
               });
             },
             items: <String>[ 'Select the Blood Type',
               'A+',
               'O+',
               'B+',
               'AB+',
               'B-',
               'A-',
               'O-',
               'AB-']
                 .map<DropdownMenuItem<String>>((String value) {
               return DropdownMenuItem<String>(
                 value: value,
                 child: Text(value),
               );
             })
                 .toList(),
           ),
         ),
   ),
       ),
     );

  }
}

