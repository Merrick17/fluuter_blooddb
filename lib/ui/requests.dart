import 'package:blooddb/providers/dropdownprovider.dart';
import 'package:division/division.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class RequestScreens extends StatelessWidget {
  String dropdownValue = 'Select the Blood Type';
  Widget RequestItem()
  {
    return  Padding(
      padding: const EdgeInsets.all(8.0),
      child: Division(
        style: StyleClass()
          ..padding(horizontal: 10, vertical: 15)
          ..background.hex('FFFFFF')
          ..borderRadius(all: 30)
          ..alignment.center()
          ..elevation(10, color: rgb(150,150,150)),

        gesture: GestureClass()
          ..onTap(() => print('Button pressed')),

        child: ListTile(
          title: Text("Donor Example"),
          subtitle: Text("20 831 241 "),
          leading: ClipRRect(
            borderRadius: BorderRadius.circular(15.0),
            child: new SizedBox(
              height: 70.0,
              width: 70.0,
              child: Container(
                color:Colors.redAccent ,
                child: new Center(child: new Text('A+',style: new TextStyle(color: Colors.white,fontSize: 23.0),),),
              ),
            ),
          ),
          trailing: Icon(Icons.phone,color: Colors.red,size: 45.0,),
          isThreeLine: true,
          onTap: () {
            print("On Tap is fired");
          },
        ),
      ),
    );
  }

  Widget RequestList() {

    return ListView(children: <Widget>[
      RequestItem(),
      RequestItem(),
      RequestItem(),
      RequestItem(),
      RequestItem(),
      RequestItem(),
      RequestItem(),
      RequestItem(),
    ],);
  }
  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<DropDownProvider>(context);
    dropdownValue = provider.getcurrentValue();
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: false,
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Stack(children: <Widget>[
          Align(
            alignment: Alignment.bottomCenter,
            child: SizedBox(
                child: Container(
                    alignment: Alignment.center,
                    height: MediaQuery.of(context).size.height/2,
                    width: double.infinity,
                    child: RequestList()
                  //color: Color(0xffde0a1e),
                )),
          ),
          SizedBox(
              child: Container(
                alignment: Alignment.center,
                height: 150,
                width: double.infinity,
                child: Text(
                  "",
                  style: TextStyle(color: Colors.white, fontSize: 30),
                ),
                color: Color(0xffde0a1e),
              )),
          Container(
            padding: const EdgeInsets.only(top: 40.0, left: 10.0, right: 10.0),
            child: Division(
              style: StyleClass()
                ..padding(horizontal: 30, vertical: MediaQuery.of(context).size.height/17)
                ..background.hex('FFFFFF')
                ..borderRadius(all: 30)
                ..alignment.center()
                ..elevation(10, color: rgb(150, 150, 150))
                ..alignment.topCenter(),
              //gesture: GestureClass,
              child: SizedBox(
                width: double.maxFinite,
                height: (MediaQuery.of(context).size.height/3.5)-10,
                child: new Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Align(
                        alignment: Alignment.topLeft,
                        child: new Text(
                          'Filter Requests',
                          style: new TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 15.0),
                        )),
                    ClipRRect(
                      borderRadius: BorderRadius.circular(15.0),
                      child: Container(
                        color: Color(0xfff5f5f5),
                        child: ListTile(
                          title: TextField(
                            decoration: new InputDecoration(
                                border: InputBorder.none,
                                focusedBorder: InputBorder.none,
                                contentPadding: EdgeInsets.only(
                                    left: 15, bottom: 11, top: 11, right: 15),
                                hintText: 'Find By Location',
                                hintStyle: new TextStyle(fontSize: 14.0)),
                          ),
                        ),
                      ),
                    ),
                    ClipRRect(
                        borderRadius: BorderRadius.circular(15.0),
                        child: Container(
                          color: Color(0xfff5f5f5),
                          child: ListTile(
                            title: DropdownButtonHideUnderline(
                              child: DropdownButton<String>(
                                value: dropdownValue,
                                isDense: true,
                                onChanged: (String newValue) {
                                  //dropdownValue = newValue;
                                  provider.currentValue(newValue);
                                },
                                items: <String>[
                                  'Select the Blood Type',
                                  'A+',
                                  'O+',
                                  'B+',
                                  'AB+',
                                  'B-',
                                  'A-',
                                  'O-',
                                  'AB-'
                                ].map<DropdownMenuItem<String>>((String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(
                                      value,
                                      style: new TextStyle(fontSize: 15.0),
                                    ),
                                  );
                                }).toList(),
                              ),
                            ),
                          ),
                        )),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(15.0),
                        child: SizedBox(
                          width: double.infinity,
                          height: 45.0,
                          child: RaisedButton(
                            color: Color(0xffde0a1e),
                            child: Text("Search",style: new TextStyle(
                                color: Colors.white,
                                fontSize: 20.0
                            ),),
                            onPressed: (){},
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),


        ]),
      ),
    );
  }
}